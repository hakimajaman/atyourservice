import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import "./assets/sass/NavbarComponent.scss";
import "./assets/sass/ProfilePicture.scss";
import { useDispatch, useSelector } from "react-redux";
//import {POST_LOGOUT_USER} from '../../store/actions/types';
import { GetMyProfile, GetAllUsers } from "../../store/actions/users";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Accordion,
  Button,
  Collapse,
  Row,
  Col,
  Navbar,
  Nav,
  Modal,
  useAccordionToggle,
} from "react-bootstrap";
import { SHOW_LOADING_DASHBOARD } from "../../store/actions/types";

const CustomToggleAccordion = ({ children, eventKey }) => {
  const decoratedOnClick = useAccordionToggle(eventKey);
  return (
    <div className="mt-2" onClick={decoratedOnClick}>
      {children}
    </div>
  );
};

const NavbarComponent = (props) => {
  //const pushTo = useHistory();
  const dispatch = useDispatch();
  const loggedStatus = useSelector((state) => state.loggedReducer);
  const userProfile = useSelector((state) => state.UserReducer.profile);
  const [openProfile, setOpenProfile] = useState(false);
  const [modalSignOut, setModalSignOut] = useState(false);
  const handleShow = () => setModalSignOut(true);
  const handleClose = () => setModalSignOut(false);

  const [profile, setProfile] = useState({
    picture: null,
    name: null,
  });

  const handleSignOut = () => {
    localStorage.removeItem("bearer");
    //pushTo.push("/login");
  };

  useEffect(() => {
    if (loggedStatus || localStorage.getItem("bearer")) {
    } else {
      //pushTo.push("/login");
    }
  }, [loggedStatus]);

  useEffect(() => {
    dispatch({
      type: SHOW_LOADING_DASHBOARD,
    });
    dispatch(GetMyProfile());
    dispatch(GetAllUsers());
  }, [dispatch]);

  useEffect(() => {
    setProfile({
      picture: userProfile ? userProfile.image : null,
      name: userProfile ? userProfile.name : null,
    });
  }, [userProfile]);

  return (
    <div>
      <Modal show={modalSignOut} onHide={handleClose}>
        <Modal.Body>Are you sure you want to sign out?</Modal.Body>
        <Modal.Footer>
          <Button variant="light" onClick={handleClose}>
            Later
          </Button>
          <Button variant="danger" onClick={handleSignOut}>
            Sign Out now
          </Button>
        </Modal.Footer>
      </Modal>
      <Navbar bg="light" expand="md">
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          {/*Sidebar*/}
          <Col xl={2} lg={3} md={3} className="customSidebar fixed-top">
            <Navbar.Brand
              href="#home"
              className="text-white d-block mx-auto text-center py-3 mb-4 customSidebar-bottomBorder"
            >
              <img
                src={require("./assets/images/bukusaku-logo.png")}
                width="30"
                height="30"
                className="d-inline-block align-top d-md-none mr-3"
                alt="logo"
              />
              atYourService
            </Navbar.Brand>
            <div
              className="customSidebar-profile customSidebar-bottomBorder pb-3"
              onClick={() => setOpenProfile(!openProfile)}
            >
              <div className="d-flex">
                <div className="mr-3 profile-picture-container">
                  <img src={profile.picture} alt="profile" />
                </div>
                <div className="d-flex align-items-center">
                  <span
                    style={{ cursor: "pointer" }}
                    className="text-white mr-3 text-capitalize"
                  >
                    {profile.name}
                  </span>
                  <span className="text-white" style={{ fontSize: "8px" }}>
                    &#x25BC;
                  </span>
                </div>
              </div>
              <Collapse in={openProfile}>
                <Nav
                  defaultActiveKey="/home"
                  className="customSidebar-nav flex-column mt-2"
                >
                  <NavLink
                    to="/admin/profile"
                    className="mt-4"
                    activeClassName="activeLinkNav"
                  >
                    <li>
                      <i>
                        <FontAwesomeIcon
                          icon={["fas", "user"]}
                          className="mr-2"
                        />
                      </i>
                      My Profile
                    </li>
                  </NavLink>
                  <li className="customSidebar-nav-sub">
                    <div className="d-flex align-items-center">
                      <i>
                        <FontAwesomeIcon
                          icon={["fas", "sign-out-alt"]}
                          className="mr-2"
                        />
                      </i>
                      Sign Out
                    </div>
                  </li>
                </Nav>
              </Collapse>
            </div>
            <Nav className="customSidebar-nav flex-column">
              <NavLink
                to="/admin/dashboard"
                className="mt-4"
                activeClassName="activeLinkNav"
              >
                <li>
                  <i>
                    <FontAwesomeIcon
                      icon={["fas", "chart-line"]}
                      className="mr-2"
                    />
                  </i>
                  Dashboard
                </li>
              </NavLink>
              <NavLink
                to="/admin/mail"
                className="mt-2"
                activeClassName="activeLinkNav"
              >
                <li>
                  <i>
                    <FontAwesomeIcon
                      icon={["fas", "envelope"]}
                      className="mr-2"
                    />
                  </i>
                  Mail
                </li>
              </NavLink>
              <NavLink
                to="/admin/business"
                className="mt-2"
                activeClassName="activeLinkNav"
              >
                <li>
                  <i>
                    <FontAwesomeIcon
                      icon={["fas", "chart-bar"]}
                      className="mr-2"
                    />
                  </i>
                  Business
                </li>
              </NavLink>
              <Accordion defaultActiveKey="0">
                <CustomToggleAccordion eventKey="/admin/article">
                  <li className="customSidebar-nav-sub">
                    <div className="d-flex align-items-center">
                      <i>
                        <FontAwesomeIcon
                          icon={["fas", "newspaper"]}
                          className="mr-2"
                        />
                      </i>
                      Article
                      <span
                        className="text-white ml-2"
                        style={{ fontSize: "8px" }}
                      >
                        &#x25BC;
                      </span>
                    </div>
                  </li>
                </CustomToggleAccordion>
                <Accordion.Collapse eventKey="/admin/article">
                  <Nav
                    defaultActiveKey="/home"
                    className="flex-column customSidebar-nav-sub-link"
                  >
                    <NavLink
                      to="/admin/add/article"
                      activeClassName="activeLinkNav"
                    >
                      <li>Add Article</li>
                    </NavLink>
                    <NavLink
                      to="/admin/article"
                      activeClassName="activeLinkNav"
                    >
                      <li>All Article</li>
                    </NavLink>
                  </Nav>
                </Accordion.Collapse>
              </Accordion>
              <NavLink
                to="/admin/users"
                className="mt-2"
                activeClassName="activeLinkNav"
              >
                <li>
                  <i>
                    <FontAwesomeIcon icon={["fas", "users"]} className="mr-2" />
                  </i>
                  Users
                </li>
              </NavLink>
            </Nav>
          </Col>
          {/*Topbar*/}
          <Col
            xl={10}
            lg={9}
            md={9}
            className="customTopbar bg-dark py-2 fixed-top ml-auto"
          >
            <Nav className="justify-content-end">
              <Navbar.Brand href="#home" className="mr-auto">
                <img
                  src={require("./assets/images/bukusaku-logo.png")}
                  width="40"
                  height="40"
                  className="d-md-inline-block align-top d-sm-none"
                  alt="logo"
                />
              </Navbar.Brand>
              <Nav.Link href="#" className="text-light mr-5 align-items-center">
                Home
              </Nav.Link>
              <Nav.Link href="#" className="text-light mr-5">
                Landing Page
              </Nav.Link>
              <Nav.Link
                onClick={handleShow}
                href="#"
                className="d-flex flex-column align-items-center mr-sm-auto mr-md-0"
              >
                <i>
                  <FontAwesomeIcon
                    icon={["fas", "sign-out-alt"]}
                    className="text-light"
                  />
                </i>
                <span className="text-light" style={{ fontSize: "8px" }}>
                  Sign Out
                </span>
              </Nav.Link>
            </Nav>
          </Col>
        </Navbar.Collapse>
      </Navbar>
      <Col xl={10} lg={9} md={9} sm={9} className="ml-md-auto ml-sm-0">
        <Row className="pt-md-5 mt-md-3 mb-5">{props.children}</Row>
      </Col>
    </div>
  );
};

export default NavbarComponent;
