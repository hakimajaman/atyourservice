import React from "react";
import NavbarComponent from "../NavbarComponent";

const MainLayout = (props) => {
  return (
    <div>
      <NavbarComponent>{props.children}</NavbarComponent>
    </div>
  );
};

export default MainLayout;
