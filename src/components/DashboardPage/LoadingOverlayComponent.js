import React from "react";
import "./assets/sass/OverlayComponent.scss";
import { Spinner } from "react-bootstrap";

const LoadingOverlayComponent = () => {
  return (
    <div
      className="d-flex flex-column align-items-center justify-content-center"
      style={{ height: "65vh", width: "80vw" }}
    >
      <Spinner
        animation="border"
        variant="primary"
        size="lg"
        className="mb-2"
      />
      <h4 className="font-weight-bold">Loading...</h4>
    </div>
  );
};

export default LoadingOverlayComponent;
