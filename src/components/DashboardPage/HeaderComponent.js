import React, {forwardRef} from 'react';
import {useHistory} from 'react-router-dom';
import {Button, FormControl, Form, Nav, Navbar, NavDropdown} from 'react-bootstrap';
import "./assets/sass/HeaderComponent.scss";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

const HeaderComponent = () => {

  const pushHistory = useHistory();

  const toggleMenu = () => {
    if (document.getElementById("mySidebar").style.width === "200px") {
      document.getElementById('mySidebar').style.width = '0';
      document.getElementById('main').style.marginLeft = '0';
    } else {
      document.getElementById("mySidebar").style.width = "200px";
      document.getElementById("main").style.marginLeft = "200px";
    }
  }

  const LogOut = () => {
    localStorage.removeItem('bearer');
    pushHistory.push('/login')
  }

  React.useEffect(() => {
    toggleMenu()
  }, [])

  return (
    <div className="border-bottom bg-light">
      <Navbar expand="lg">
        <Navbar.Brand onClick={toggleMenu} style={{cursor: 'pointer'}}>
          <div className="bar1"></div>
          <div className="bar2"></div>
          <div className="bar3"></div>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link className="mr-4">Home</Nav.Link>
            <Nav.Link onClick={LogOut}>
              <FontAwesomeIcon icon={['fas', 'door-open']} />
              Log Out
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  )
}

export default HeaderComponent;
