import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Button, Table } from "react-bootstrap";

const TableArticle = () => {
  const allArticles = useSelector((state) => state.ArticleReducer);

  const table = () => {
    return allArticles.allArticles.docs.map((item, index) => (
      <tr>
        <td>{index + 1}</td>
        <td>{item.title}</td>
        <td>
          {item.body.length > 23 ? item.body.slice(0, 20) + "..." : item.body}
        </td>
        <td>{item.category[0]}</td>
        <td className="text-right">{item.likes.length} Likes</td>
        <td>
          <Link to={`/admin/edit/article/${item._id}`} className="mr-4">
            Edit
          </Link>
          <Link to="#">Remove</Link>
        </td>
      </tr>
    ));
  };

  return (
    <div>
      <div className="mb-3">
        <Button variant="danger">Add New Article</Button>
      </div>
      <div
        className="bg-light p-3 border-danger rounded"
        style={{
          width: "80vw",
          borderTop: "4px solid",
          borderBottom: "4px solid",
        }}
      >
        <h4>All Articles</h4>
        <Table responsive hover bordered size="sm">
          <thead style={{ fontSize: "13px" }}>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>Description</th>
              <th>Category</th>
              <th className="text-right">Total Likes</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody style={{ fontSize: "13px" }}>{table()}</tbody>
        </Table>
      </div>
    </div>
  );
};

export default TableArticle;
