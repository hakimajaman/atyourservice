import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {GetAllInquiries, GetInquiryById} from '../../../store/actions/inquiry';
import {GetAllBusiness} from '../../../store/actions/business';
import {Tab, Row, Col, Nav} from 'react-bootstrap';

const TableInquiry = () => {
  const dispatch = useDispatch();
  const allInquiries = useSelector(state => state.InquiryReducer);
  const users = useSelector(state => state.UserReducer);
  const business = useSelector(state => state.BusinessReducer);

  useEffect(() => {
    dispatch(GetAllInquiries());
    dispatch(GetAllBusiness());
  }, [dispatch])

  const userInquiries = () => {
    if (allInquiries.allInquiries !== undefined && users.allUsers !== undefined) {
      return (
        allInquiries.allInquiries.docs.sort((itemA, itemB) => itemA.isRead === itemB.isRead ? 0 : itemA.isRead ? 1 : -1).map(item =>
          <Nav.Item className="border">
            <Nav.Link eventKey={item._id} onClick={() => dispatch(GetInquiryById(item._id))}>
              <div className="d-flex align-items-center mt-1">
                <div className="profile-picture-container">
                  <img src={users.allUsers.message.data.docs.find(_item => _item._id === item.owner_id)?.image} alt="profile" />
                </div>
                <span className="ml-3 text-muted">{users.allUsers.message.data.docs.find(_item => _item._id === item.owner_id)?.name}</span>
              </div>
              <div className="ml-5 pl-4">
                <p className={`${
                  item.isRead === false
                    ?
                    "font-weight-bold text-dark"
                    :
                    "text-dark"
                  }`}>
                  {
                    item.description.length >= 74
                      ?
                      item.description.slice(0, 74) + "... "
                      :
                      item.description
                  }
                </p>
              </div>
            </Nav.Link>
          </Nav.Item>
        )
      )
    } else {return null}
  }

  const requestInquiries = () => {
    if (allInquiries.allInquiries !== undefined && users.allUsers !== undefined && business.allBusiness !== undefined) {
      return (
        allInquiries.allInquiries.docs.map(item =>
          <Tab.Pane eventKey={item._id}>
            <div className="d-flex align-items-center bg-dark p-3 border-bottom">
              <div className="profile-picture-container2">
                <img src={users.allUsers.message.data.docs.find(_item => _item._id === item.owner_id)?.image} alt="profile" />
              </div>
              <span className="ml-3 text-light">{users.allUsers.message.data.docs.find(_item => _item._id === item.owner_id)?.name}</span>
              {
                item.isRead === true
                  ?
                  <span className="ml-auto badge badge-success py-2">
                    Approved
                  </span>
                  :
                  <span className="ml-auto badge badge-warning py-2">
                    Pending
                  </span>
              }
            </div>
            <div className="px-5 mt-3">
              <h3>{item.name}</h3>
              <hr className="bg-dark" />
              <p>{item.description}</p>
            </div>
            <div>
              <hr className="bg-dark" />
              <div className="px-3 d-flex">
                <div className="mr-4">
                  <div className="mb-1 text-muted d-flex align-items-center">
                    <FontAwesomeIcon icon={['fas', 'at']} />
                    <span className="ml-2">{item.email}</span>
                  </div>
                  <div className="text-muted d-flex align-items-center">
                    <FontAwesomeIcon icon={['fas', 'phone']} />
                    <span className="ml-2">{item.phone}</span>
                  </div>
                </div>
                <div className="mb-2">
                  <div className="mb-1 text-muted d-flex align-items-center">
                    <FontAwesomeIcon icon={['fas', 'user-md']} />
                    <span className="ml-2">{item.job}</span>
                  </div>
                  <div className="mb-1 text-muted d-flex align-items-center">
                    <FontAwesomeIcon icon={['fas', 'chart-bar']} />
                    <Link to={
                      `/admin/business/${
                      item.business_id
                      }`
                    }>
                      <span className="ml-2">
                        {
                          business.allBusiness.find(__item =>
                            __item._id === item.business_id)?.company
                        }
                      </span>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </Tab.Pane>
        )
      )
    }
  }

  return (
    <div>
      <Tab.Container id="left-tabs-example" defaultActiveKey="first">
        <Row>
          <Col sm={6} className="mr-0 mb-3 border p-3">
            <Nav className="flex-column border">
              {userInquiries()}
            </Nav>
          </Col>
          <Col sm={5} className="border ml-2 p-0" style={{height: '100%'}}>
            <Tab.Content>
              {requestInquiries()}
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    </div>
  );
};

export default TableInquiry;
