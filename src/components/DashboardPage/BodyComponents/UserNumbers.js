import React, { useState } from "react";
import { useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Card } from "react-bootstrap";

const UserNumbers = () => {
  const totalUsers = useSelector((state) =>
    state.UserReducer.allUsers ? state.UserReducer.allUsers : state.UserReducer
  );
  const allReports = useSelector(
    (state) => state.ReportReducer.allReports.docs.length
  );

  const [userNumbers] = useState({
    registered: totalUsers.message.total ? totalUsers.message.total : 0,
    verificated: totalUsers.message.verificated
      ? totalUsers.message.verificated
      : 0,
    notVerificated: totalUsers.message.notVerificated
      ? totalUsers.message.notVerificated
      : 0,
  });

  return (
    <div className="d-xl-flex d-lg-flex ml-3 ml-sm-6">
      <Card className="customGrid-item mr-3 mb-3">
        <Card.Body className="d-flex justify-content-between">
          <div>
            <i className="fa-3x text-primary">
              <FontAwesomeIcon icon={["fas", "users"]} />
            </i>
          </div>
          <div className="text-right text-secondary ml-2">
            <h5>Registered Users</h5>
            <h3>{userNumbers.registered}</h3>
          </div>
        </Card.Body>
      </Card>
      <Card className="customGrid-item mr-3 mb-3">
        <Card.Body className="d-flex justify-content-between">
          <div>
            <i className="fa-3x text-success">
              <FontAwesomeIcon icon={["fas", "users"]} />
            </i>
          </div>
          <div className="text-right text-secondary ml-2">
            <h5>Verified Users</h5>
            <h3>{userNumbers.verificated}</h3>
          </div>
        </Card.Body>
      </Card>
      <Card className="customGrid-item mr-3 mb-3">
        <Card.Body className="d-flex justify-content-between">
          <div>
            <i className="fa-3x text-danger">
              <FontAwesomeIcon icon={["fas", "users"]} />
            </i>
          </div>
          <div className="text-right text-secondary ml-2">
            <h5>Not Verified Users</h5>
            <h3>{userNumbers.notVerificated}</h3>
          </div>
        </Card.Body>
      </Card>
      <Card className="customGrid-item mr-3 mb-3">
        <Card.Body className="d-flex justify-content-between">
          <div>
            <i className="fa-3x text-warning">
              <FontAwesomeIcon icon={["fas", "envelope"]} />
            </i>
          </div>
          <div className="text-right text-secondary ml-2">
            <h5>Total Inbox</h5>
            <h3>{allReports}</h3>
          </div>
        </Card.Body>
      </Card>
    </div>
  );
};

export default UserNumbers;
