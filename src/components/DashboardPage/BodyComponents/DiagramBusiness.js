import React, { useState, useEffect } from "react";
import { Line } from "react-chartjs-2";

const DiagramBusiness = () => {
  const [chart, setChart] = useState({});
  const charts = () => {
    setChart({
      labels: ["monday", "tuesday", "wednesday", "thursday", "friday"],
      datasets: [
        {
          label: "Business Chart",
          data: [13, 23, 32, 11, 12],
          backgroundColor: ["rgba(75, 192, 192, 0.6)"],
          borderWidth: 4,
        },
      ],
    });
  };
  useEffect(() => {
    charts();
  }, []);
  return (
    <div>
      <h4>Business Chart</h4>
      <div style={{ width: "40rem" }}>
        <Line data={chart} options={{ responsive: true }} />
      </div>
    </div>
  );
};

export default DiagramBusiness;
