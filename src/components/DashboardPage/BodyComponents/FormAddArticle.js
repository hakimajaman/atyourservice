import React, { useState } from "react";
import { url } from "../../../store/actions/types";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import ReactHtmlParser from "react-html-parser";
import { Button } from "react-bootstrap";

const FormAddArticle = () => {
  const [value, setValue] = useState("");
  const [image, setImage] = useState();
  const [subject, setSubject] = useState({
    title: "",
    category: "",
  });
  const handleChange = (e, editor) => {
    const data = editor.getData();
    setValue(data);
  };
  const handleChangeSubject = (e) => {
    setSubject({
      ...subject,
      [e.target.name]: e.target.value,
    });
  };
  const handleChangeImage = (e) => {
    setImage(e.target.files[0]);
  };
  const PostArticle = () => async (dispatch) => {
    try {
      const formData = new FormData();
      formData.append("title", subject.title);
      formData.append("body", value);
      formData.append("category", subject.category);
      formData.append("image", image);
      console.log(image);
      console.log(formData.values());
      const send = await fetch(`${url}/article/addArticle`, {
        method: "POST",
        headers: {
          Authorization: localStorage.getItem("bearer"),
        },
        body: formData,
      });
      const res = await send.json();
      console.log(res);
    } catch (err) {
      console.log(err);
    }
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    PostArticle()();
  };
  return (
    <div className="d-flex flex-column flex-md-row">
      <div className="mb-4 mr-3">
        <div>
          <h4>Add New Article Here</h4>
        </div>
        <div className="d-flex mb-2">
          <input
            type="text"
            value={subject.title}
            name="title"
            onChange={handleChangeSubject}
            placeholder="Article Title"
          />
          <input
            type="text"
            name="category"
            value={subject.category}
            onChange={handleChangeSubject}
            placeholder="Article Categories"
          />
          <Button onClick={handleSubmit}>Add Now</Button>
        </div>
        <input
          type="file"
          onChange={(e) => handleChangeImage(e)}
          name="image"
        />
        <CKEditor editor={ClassicEditor} onChange={handleChange} />
      </div>
      <div>
        <h4>Live Preview</h4>
        <div
          className="border border-dark bg-light"
          style={{ width: "20rem", height: "28rem", borderRadius: "10px" }}
        >
          <div
            className="bg-dark text-light text-center mb-3"
            style={{
              borderTopLeftRadius: "10px",
              borderTopRightRadius: "10px",
            }}
          >
            <span>My Phone</span>
          </div>
          <div style={{ overflow: "scroll", height: "100%", width: "100%" }}>
            <div className="text-center border-bottom">
              <h4>{ReactHtmlParser(subject.title)}</h4>
            </div>
            <div className="ml-2">{ReactHtmlParser(value)}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAddArticle;
