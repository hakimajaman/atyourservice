import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "../../assets/sass/DashboardComponent.scss";
import UserNumbers from "../UserNumbers";
import MostArticle from "../MostArticle";
import DiagramBusiness from "../DiagramBusiness";
import LoadingOverlayComponent from "../../LoadingOverlayComponent";
import { GetAllUsers } from "../../../../store/actions/users";
import { GetAllReports } from "../../../../store/actions/report";
import { GetAllArticles } from "../../../../store/actions/article";

const DashboardComponent = () => {
  const dispatch = useDispatch();
  const allUsers = useSelector((state) => state.UserReducer);
  const allReports = useSelector((state) => state.ReportReducer);
  const allArticles = useSelector((state) => state.ArticleReducer);

  useEffect(() => {
    dispatch(GetAllUsers());
    dispatch(GetAllReports());
    dispatch(GetAllArticles());
  }, [dispatch]);

  return (
    <div className="d-flex flex-column">
      {allUsers.allUsers && allReports.allReports && allArticles.allArticles ? (
        <div>
          <h2 className="ml-4">Dashboard</h2>
          <UserNumbers />
          <div className="ml-4 d-flex justify-content-center">
            <div className="mr-4">
              <DiagramBusiness />
            </div>
            <div>
              <MostArticle />
            </div>
          </div>
        </div>
      ) : (
        <LoadingOverlayComponent />
      )}
    </div>
  );
};

export default DashboardComponent;
