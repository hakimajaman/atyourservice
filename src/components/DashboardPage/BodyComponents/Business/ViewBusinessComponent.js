import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { GetBusinessById } from "../../../../store/actions/business";
import LoadingOverlayComponent from "../../LoadingOverlayComponent";
import ReadBusiness from "../ReadBusiness";

const ViewBusinessComponent = () => {
  const dispatch = useDispatch();
  const id = useParams();
  const thisBusiness = useSelector((state) => state.BusinessReducer);

  useEffect(() => {
    dispatch(GetBusinessById(id));
  }, [dispatch, id]);

  return (
    <div className="d-flex flex-column ml-4 mr-1">
      {thisBusiness.business ? (
        <div>
          <h4>Business Request</h4>
          <ReadBusiness />
        </div>
      ) : (
        <LoadingOverlayComponent />
      )}
    </div>
  );
};

export default ViewBusinessComponent;
