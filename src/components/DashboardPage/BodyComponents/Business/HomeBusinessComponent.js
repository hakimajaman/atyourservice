import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GetAllBusiness } from "../../../../store/actions/business";
import TableBusiness from "../TableBusiness";
import LoadingOverlayComponent from "../../LoadingOverlayComponent";

const HomeBusinessComponent = () => {
  const dispatch = useDispatch();
  const allBusiness = useSelector((state) => state.BusinessReducer);
  const [showTable, setShowTable] = useState(false);

  useEffect(() => {
    dispatch(GetAllBusiness());
  }, [dispatch]);

  useEffect(() => {
    if (allBusiness.allBusiness !== undefined) {
      setShowTable(true);
    }
  }, [allBusiness.allBusiness]);

  return (
    <div className="d-flex flex-column ml-4 mr-1">
      {showTable ? (
        <div>
          <div>
            <TableBusiness />
          </div>
        </div>
      ) : (
        <LoadingOverlayComponent />
      )}
    </div>
  );
};

export default HomeBusinessComponent;
