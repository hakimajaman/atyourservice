import React, { useEffect } from "react";
import MailBusiness from "../MailBusiness";
import LoadingOverlayComponent from "../../LoadingOverlayComponent";
import { useDispatch, useSelector } from "react-redux";
import { GetAllReports } from "../../../../store/actions/report";
import { GetAllUsers } from "../../../../store/actions/users";

const MailBusinessComponent = () => {
  const dispatch = useDispatch();
  const reportReducer = useSelector((state) => state.ReportReducer);
  const userReducer = useSelector((state) => state.UserReducer);

  useEffect(() => {
    dispatch(GetAllReports());
    dispatch(GetAllUsers());
  }, [dispatch]);

  return (
    <div className="ml-3 ml-sm-6">
      {reportReducer.allReports && userReducer.allUsers ? (
        <MailBusiness />
      ) : (
        <LoadingOverlayComponent />
      )}
    </div>
  );
};

export default MailBusinessComponent;
