import React, { useState, useRef } from "react";
import "../assets/sass/Hover.scss";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import useOutside from "../OutSideClickFunction";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  ApproveBusiness,
  DeleteBusiness,
} from "../../../store/actions/business";
import { Table, Button, Modal, Tooltip, OverlayTrigger } from "react-bootstrap";

const TableBusiness = () => {
  const dispatch = useDispatch();
  const dropdownMenuRef = useRef();
  const allBusiness = useSelector((state) => state.BusinessReducer);
  const [modalDelete, setModalDelete] = useState(false);
  const [idBusiness, setIdBusiness] = useState("");
  const [dropdownMoreAction, setDropdownMoreAction] = useState({
    id: null,
    action: false,
  });
  const ApproveIt = (id) => {
    dispatch(ApproveBusiness(id));
  };
  const handleShow = (id) => {
    setModalDelete(true);
    setIdBusiness(id);
  };
  const handleClose = () => setModalDelete(false);
  const deleteBusiness = () => {
    dispatch(DeleteBusiness(idBusiness));
    setModalDelete(false);
  };
  const toolTipIcon = (text) => {
    return <Tooltip id="button-tooltip">{text}</Tooltip>;
  };

  let idDropdown = null;
  const dropDownMoreAction = (id) => {
    if (dropdownMoreAction.action === false) {
      setDropdownMoreAction({
        id: id,
        action: true,
      });
      idDropdown = id;
    } else {
      setDropdownMoreAction({
        id: id,
        action: false,
      });
      setDropdownMoreAction({
        id: id,
        action: true,
      });
      idDropdown = id;
    }
  };

  const handleBlur = () => {
    setDropdownMoreAction({
      id: idDropdown,
      action: false,
    });
    setDropdownMoreAction({
      id: idDropdown,
      action: true,
    });
  };

  useOutside(dropdownMenuRef, handleBlur, dropdownMoreAction.action);

  let num = [];

  const businessTable = () => {
    return allBusiness.allBusiness.map((item) => (
      <tr>
        <td className="border-right">{num.push(item._id)}</td>
        <td className="border-right">
          <img src={item.logo} alt="logo" width="30" />
        </td>
        <td className="border-right">
          {item.name.length >= 12 ? item.name.slice(0, 12) + "..." : item.name}
        </td>
        <td className="text-uppercase border-right">
          {item.company.length >= 20
            ? item.company.slice(0, 20) + "..."
            : item.company}
        </td>
        <td className="border-right">
          {item.description.length > 30
            ? item.description.slice(0, 30) + "..."
            : item.description}
        </td>
        <td className="border-right">{item.category}</td>
        <td>
          {item.isApproved === true ? (
            <span className="badge badge-success py-2">Approved</span>
          ) : (
            <span className="badge badge-warning py-2">Pending</span>
          )}
        </td>
        <td>
          <OverlayTrigger
            placement="left"
            delay={{ show: 250, hide: 400 }}
            overlay={toolTipIcon("Read More")}
          >
            <Link to={`/admin/business/${item._id}`} className="text-dark">
              <i
                className="ml-2 border rounded-circle p-2 text-dark"
                style={{ cursor: "pointer" }}
              >
                <FontAwesomeIcon icon={["fas", "eye"]} />
              </i>
            </Link>
          </OverlayTrigger>
        </td>
        <td>
          <OverlayTrigger
            placement="right"
            delay={{ show: 250, hide: 400 }}
            overlay={toolTipIcon("More Actions")}
          >
            <i
              className="ml-2 border rounded-circle p-2"
              style={{ cursor: "pointer" }}
              onClick={() => dropDownMoreAction(item._id)}
            >
              <FontAwesomeIcon icon={["fas", "ellipsis-h"]} />
            </i>
          </OverlayTrigger>
          {dropdownMoreAction.id === item._id &&
          dropdownMoreAction.action === true ? (
            <div
              style={{ position: "absolute", marginLeft: "2rem" }}
              className="border bg-light border-muted rounded pl-1 pr-4 py-2"
              ref={dropdownMenuRef}
            >
              <span className="span-hover" onClick={() => ApproveIt(item._id)}>
                Approve
              </span>
              <br className="mb-2" />
              <span className="span-hover" onClick={() => handleShow(item._id)}>
                Remove
              </span>
            </div>
          ) : undefined}
        </td>
      </tr>
    ));
  };

  return (
    <div>
      <Modal show={modalDelete} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are sure you want to delete it?</Modal.Body>
        <Modal.Footer>
          <Button variant="light" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="danger" onClick={deleteBusiness}>
            Delete Now
          </Button>
        </Modal.Footer>
      </Modal>
      <div>
        <h4>All Business</h4>
        <div
          className="mb-3 bg-light p-3 border-danger rounded"
          style={{ border: "4px solid" }}
        >
          <Table hover size="md" responsive="sm">
            <thead>
              <tr className="text-bold">
                <th>#</th>
                <th>Logo</th>
                <th>Name</th>
                <th>Company</th>
                <th>Description</th>
                <th>Category</th>
                <th>Status</th>
                <th />
                <th />
              </tr>
            </thead>
            <tbody style={{ fontSize: "14px" }}>{businessTable()}</tbody>
          </Table>
        </div>
      </div>
    </div>
  );
};

export default TableBusiness;
