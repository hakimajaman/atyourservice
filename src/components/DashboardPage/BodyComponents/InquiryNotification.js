import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {GetAllInquiries} from '../../../store/actions/inquiry';
import {GetAllBusiness} from '../../../store/actions/business';
import {Button, Card, Pagination, Accordion} from 'react-bootstrap';

const InquiryNotification = () => {

  const dispatch = useDispatch();
  const allInquiries = useSelector(state => state.InquiryReducer);
  const businessId = useSelector(state => state.BusinessReducer);
  const [totalUnread, setTotalUnread] = useState();

  useEffect(() => {
    dispatch(GetAllInquiries());
    dispatch(GetAllBusiness());
  }, [dispatch])

  useEffect(() => {
    if (allInquiries.allInquiries !== undefined) {
      setTotalUnread(allInquiries.allInquiries.docs.filter(item => item.isRead === false).length)
    }
  }, [allInquiries.allInquiries])

  const totalInquiries = () => {
    if (totalUnread === 1) {
      return (
        <h3 className="text-center mb-3 h3">You have {totalUnread} new Inquiry</h3>
      )
    } else if (totalUnread > 1) {
      return (
        <h3 className="text-center mb-3 h3">You have {totalUnread} new Inquiries</h3>
      )
    } else if (totalUnread <= 0) {
      return null
    }
  }

  const unReadInquiries = () => {
    if (allInquiries.allInquiries !== undefined && businessId.allBusiness !== undefined) {
      return (
        allInquiries.allInquiries.docs.filter(item => item.isRead === false).map(item =>
          <Accordion>
            <Card className="mr-3 mb-3">
              <Card.Header className="bg-info">
                <Link to={`/admin/business/${item.business_id}`} className="text-light">
                  {
                    businessId.allBusiness.find(_item =>
                      _item._id === item.business_id
                    )?.name
                  }
                </Link>
              </Card.Header>
              <Accordion.Toggle as={Card.Body} eventKey={item._id}
                style={{cursor: 'pointer'}}>
                <img
                  src={require('../assets/images/Screenshot from 2019-10-24 11-20-53.png')}
                  alt="business-profile"
                  width="30"
                  height="30"
                />
                <span className="text-uppercase font-weight-bold ml-3">
                  You have a new Inquiry from {item.name}
                </span>
                <Accordion.Collapse eventKey={item._id}>
                  <Card
                    className="mt-3" style={{cursor: 'auto'}}>
                    <Card.Body>
                      <p>{item.description}</p>
                    </Card.Body>
                    <Card.Footer>
                      <Link to="#">
                        <Button>Read More</Button>
                      </Link>
                    </Card.Footer>
                  </Card>
                </Accordion.Collapse>
              </Accordion.Toggle>
            </Card>
          </Accordion>
        )
      )
    }
  }

  return (
    <div className="ml-3 ml-sm-6">
      <div className="col-md-9 border p-3 rounded">
        {totalInquiries()}
        {unReadInquiries()}
        <Pagination className="justify-content-end mr-3">
          <Pagination.Prev />
          <Pagination.Item>1</Pagination.Item>
          <Pagination.Item>2</Pagination.Item>
          <Pagination.Item active>3</Pagination.Item>
          <Pagination.Next />
        </Pagination>
      </div>
    </div>
  )
}

export default InquiryNotification;
