import React from "react";
import { useSelector } from "react-redux";
import ReactHtmlParser from "react-html-parser";

const MostArticle = () => {
  const mostArticle = useSelector((state) =>
    state.ArticleReducer.allArticles.docs.find(
      (item) =>
        item.likes.length ===
        Math.max.apply(
          Math,
          state.ArticleReducer.allArticles.docs.map(function (o) {
            return o.likes.length;
          })
        )
    )
  );

  return (
    <div>
      <h4>Most Popular Article</h4>
      <div
        className="border border-dark bg-light"
        style={{ width: "20rem", height: "29rem", borderRadius: "10px" }}
      >
        <div
          className="bg-dark text-light text-center mb-3"
          style={{
            borderTopLeftRadius: "10px",
            borderTopRightRadius: "10px",
          }}
        >
          <span>My Phone</span>
        </div>
        <div
          style={{
            overflow: "scroll",
            overflowX: "hidden",
            height: "90%",
            width: "100%",
          }}
        >
          <div className="text-center">
            <div className="thumbnail-picture ml-2 mr-2 mb-3">
              <img src={mostArticle.thumbnail} alt="thumbnail" />
            </div>
            <div className="customBadge d-flex align-items-center justify-content-center p-1 mb-3 ml-2">
              {mostArticle.category[0]}
            </div>
          </div>
          <h6 className="ml-2 border-bottom border-dark pb-3">
            {ReactHtmlParser(mostArticle.title)}
          </h6>
          <div className="ml-2" style={{ fontSize: "12px" }}>
            {ReactHtmlParser(mostArticle.body)}
          </div>
        </div>
      </div>
    </div>
  );
};

export default MostArticle;
