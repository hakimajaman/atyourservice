import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import FormArticle from "../FormArticle";
import LoadingOverlayComponent from "../../LoadingOverlayComponent";
import { GetArticleById } from "../../../../store/actions/article";
import { useParams } from "react-router-dom";

const EditArticleComponent = () => {
  const id = useParams();
  const dispatch = useDispatch();
  const article = useSelector((state) => state.ArticleReducer);
  useEffect(() => {
    dispatch(GetArticleById(id));
  }, [dispatch, id]);
  return (
    <div className="d-flex flex-column ml-4 mr-1">
      {article.article ? <FormArticle /> : <LoadingOverlayComponent />}
    </div>
  );
};

export default EditArticleComponent;
