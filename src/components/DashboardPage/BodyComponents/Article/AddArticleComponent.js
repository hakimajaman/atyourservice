import React from "react";
import FormAddArticle from "../FormAddArticle";

const AddArticleComponent = () => {
  return (
    <div className="d-flex flex-column ml-4 mr-1">
      <FormAddArticle />
    </div>
  );
};

export default AddArticleComponent;
