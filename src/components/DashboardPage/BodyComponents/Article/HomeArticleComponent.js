import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import LoadingOverlayComponent from "../../LoadingOverlayComponent";
import TableArticle from "../TableArticle";
import { GetAllArticles } from "../../../../store/actions/article";

const HomeArticleComponent = () => {
  const dispatch = useDispatch();
  const allArticles = useSelector((state) => state.ArticleReducer);

  useEffect(() => {
    dispatch(GetAllArticles());
  }, [dispatch]);

  return (
    <div className="d-flex flex-column ml-4 mr-1">
      {allArticles.allArticles ? <TableArticle /> : <LoadingOverlayComponent />}
    </div>
  );
};

export default HomeArticleComponent;
