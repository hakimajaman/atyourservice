import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Table, Row, Col, Pagination, Tab, Nav } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { GetReportById } from "../../../store/actions/report";

const MailBusiness = () => {
  const dispatch = useDispatch();
  const allReports = useSelector((state) => state.ReportReducer);
  const allUsers = useSelector((state) => state.UserReducer);
  const readMessage = (id) => {
    dispatch(GetReportById(id));
  };

  const mailbox = () => {
    return allReports.allReports.docs.map((item) => (
      <tr className="d-flex align-items-center">
        <input type="checkbox" />
        <Nav.Link
          eventKey={item._id}
          className="d-flex text-dark"
          style={{ fontSize: "14px" }}
          onClick={() => readMessage(item._id)}
        >
          <span className="mr-4 text-capitalize">
            {allUsers.allUsers.message.data.docs.find(
              (_item) => _item._id === item.owner
            )?.name.length > 20
              ? allUsers.allUsers.message.data.docs
                  .find((_item) => _item._id === item.owner)
                  ?.name.slice(0, 17) + "..."
              : allUsers.allUsers.message.data.docs.find(
                  (_item) => _item._id === item.owner
                )?.name}
          </span>
          {item.isRead === false ? (
            <span className="mr-1 font-weight-bold">
              {item.title.length > 14 ? item.title.slice(0, 11) : item.title}
            </span>
          ) : (
            <span className="mr-1">
              {item.title.length > 14 ? item.title.slice(0, 11) : item.title}
            </span>
          )}
          <span className="mr-1">-</span>
          {item.isRead === false ? (
            <span className="mr-5 font-weight-bold">
              {item.description.length > 40
                ? item.description.slice(0, 40) + "..."
                : item.description}
            </span>
          ) : (
            <span className="mr-5">
              {item.description.length > 40
                ? item.description.slice(0, 40) + "..."
                : item.description}
            </span>
          )}
          <span>{item.createdAt.slice(0, 10)}</span>
        </Nav.Link>
      </tr>
    ));
  };

  const mailread = () => {
    return allReports.allReports.docs.map((item) => (
      <Tab.Pane eventKey={item._id}>
        <div
          className="bg-light border-primary rounded mt-3 p-3"
          style={{ borderTop: "4px solid", borderBottom: "4px solid" }}
        >
          <div className="d-flex align-items-center">
            <h3 className="mr-2 font-weight-bold">Subject:</h3>
            <h5>{item.title}</h5>
          </div>
          <div className="d-flex border-bottom align-items-center">
            <p>
              From:{" "}
              {
                allUsers.allUsers.message.data.docs.find(
                  (_item) => _item._id === item.owner
                )?.email
              }
            </p>
            <span
              className="ml-auto text-muted mr-1"
              style={{ fontSize: "12px" }}
            >
              {item.createdAt.slice(0, 10)}
            </span>
          </div>
          <div className="d-flex justify-content-center align-items-center border-bottom">
            <i style={{ cursor: "pointer" }}>
              <FontAwesomeIcon icon={["fas", "trash-alt"]} />
            </i>
          </div>
          <p className="mt-3">{item.description}</p>
        </div>
      </Tab.Pane>
    ));
  };

  return (
    <div>
      <Tab.Container id="left-tabs-example" defaultActiveKey="first">
        <div style={{ width: "80vw" }}>
          <Row>
            <Col sm={3}>
              <div
                className="d-flex border-warning p-3 rounded bg-light"
                style={{
                  borderLeft: "4px solid",
                }}
              >
                <i className="mr-2">
                  <FontAwesomeIcon icon={["fas", "inbox"]} />
                </i>
                <span>Inbox</span>
              </div>
            </Col>
            <Col sm={9}>
              <div
                className="bg-light p-2 border-primary rounded"
                style={{ borderTop: "4px solid", borderRight: "4px solid" }}
              >
                <div className="border-bottom">
                  <i className="mr-2">
                    <FontAwesomeIcon icon={["fas", "inbox"]} />
                  </i>
                  <span>Inbox</span>
                </div>
                <div className="d-flex mt-2 border-bottom">
                  <div className="d-flex flex-column mr-3">
                    <input type="checkbox" id="checkAll" className="mt-1" />
                    <label for="checkAll" style={{ fontSize: "8px" }}>
                      Check All
                    </label>
                  </div>
                  <i style={{ cursor: "pointer" }}>
                    <FontAwesomeIcon icon={["fas", "trash-alt"]} />
                  </i>
                  <div className="ml-auto">
                    <Pagination size="sm">
                      <Pagination.Prev />
                      <Pagination.Item active>1</Pagination.Item>
                      <Pagination.Item>2</Pagination.Item>
                      <Pagination.Item>3</Pagination.Item>
                      <Pagination.Next />
                    </Pagination>
                  </div>
                </div>
                <Table striped hover responsive className="mt-2">
                  <tbody>{mailbox()}</tbody>
                </Table>
              </div>
            </Col>
          </Row>
          <div>
            <Tab.Content>{mailread()}</Tab.Content>
          </div>
        </div>
      </Tab.Container>
    </div>
  );
};

export default MailBusiness;
