import React, { useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { GetBusinessById } from "../../../store/actions/business";
import { Form, Col, Button } from "react-bootstrap";

const FormBusiness = () => {
  const dispatch = useDispatch();
  const id = useParams();
  const thisBusiness = useSelector((state) => state.BusinessReducer);

  useEffect(() => {
    dispatch(GetBusinessById(id));
  }, [dispatch, id]);

  return (
    <div>
      <Form>
        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label>Business Id</Form.Label>
            <Form.Control
              value={thisBusiness.business ? thisBusiness.business._id : null}
              placeholder="Business Id"
            />
          </Form.Group>

          <Form.Group as={Col}>
            <Form.Label>Status</Form.Label>
            <Form.Control
              value={
                thisBusiness.business &&
                thisBusiness.business.isApproved === true
                  ? "Approved"
                  : "Pending"
              }
              className={
                thisBusiness.business &&
                thisBusiness.business.isApproved === true
                  ? "text-success"
                  : "text-danger"
              }
              placeholder="Status"
            />
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label>Name</Form.Label>
            <Form.Control
              value={thisBusiness.business ? thisBusiness.business.name : null}
              placeholder="Name"
            />
          </Form.Group>

          <Form.Group as={Col}>
            <Form.Label>Company</Form.Label>
            <Form.Control
              value={
                thisBusiness.business ? thisBusiness.business.company : null
              }
              placeholder="Company name"
            />
          </Form.Group>

          <Form.Group as={Col}>
            <Form.Label>Category</Form.Label>
            <Form.Control
              value={
                thisBusiness.business ? thisBusiness.business.category : null
              }
              placeholder="Category"
            />
          </Form.Group>
        </Form.Row>

        <Form.Group>
          <Form.Label>Description</Form.Label>
          <Form.Control
            as="textarea"
            rows="3"
            value={
              thisBusiness.business ? thisBusiness.business.description : null
            }
            style={{ resize: "none" }}
            placeholder="Description"
          />
        </Form.Group>

        <Form.Row>
          <Form.Group as={Col}>
            <div className="d-flex align-items-center">
              <i className="mr-3 text-muted">
                <FontAwesomeIcon
                  icon={["fab", "google-play"]}
                  className="fa-lg"
                />
              </i>
              <Form.Control
                value={
                  thisBusiness.business
                    ? thisBusiness.business.gplay_link
                    : null
                }
                placeholder="Google Play Link"
              />
            </div>
          </Form.Group>
          <Form.Group as={Col}>
            <div className="d-flex align-items-center">
              <i className="mr-3 text-muted">
                <FontAwesomeIcon icon={["fas", "link"]} className="fa-lg" />
              </i>
              <Form.Control
                value={
                  thisBusiness.business ? thisBusiness.business.web_link : null
                }
                placeholder="Website"
              />
            </div>
          </Form.Group>
        </Form.Row>
      </Form>
    </div>
  );
};

export default FormBusiness;
