import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { GetAllUsers } from "../../../store/actions/users";

const ReadBusiness = () => {
  const dispatch = useDispatch();
  const business = useSelector((state) => state.BusinessReducer);
  const user = useSelector((state) => state.UserReducer);

  useEffect(() => {
    dispatch(GetAllUsers());
  }, [dispatch]);

  return (
    <div
      className="rounded bg-light p-3 border border-primary"
      style={{ width: "80vw", height: "100%" }}
    >
      <div className="bg-danger p-2 d-flex align-items-center rounded">
        <div className="profile-picture-container2 mr-2">
          {user.allUsers && business.business ? (
            <img
              src={
                user.allUsers.message.data.docs.filter(
                  (_item) => _item._id === business.business.owner_id
                )[0].image
              }
              alt="profile"
            />
          ) : null}
        </div>
        <span className="text-light text-capitalize">
          {user.allUsers && business.business
            ? user.allUsers.message.data.docs.filter(
                (_item) => _item._id === business.business.owner_id
              )[0].name
            : null}
        </span>
      </div>
      <div className="border-bottom p-2 mb-2">
        <div className="d-flex">
          <div className="d-flex flex-column">
            <span>
              From:
              {user.allUsers && business.business
                ? user.allUsers.message.data.docs.filter(
                    (_item) => _item._id === business.business.owner_id
                  )[0].email
                : null}
            </span>
            <span>
              Company:
              {business.business ? business.business.company : null}
            </span>
            <span>
              Category:
              {business.business ? business.business.category : null}
            </span>
            <span className="d-flex mr-2">
              <i className="text-muted">
                <FontAwesomeIcon
                  icon={["fab", "google-play"]}
                  className="fa-lg"
                />
              </i>
              <a
                href={`${
                  business.business ? business.business.gplay_link : "#"
                }`}
              >
                Download Now
              </a>
            </span>
            <span className="d-flex mr-2">
              <i className="text-muted">
                <FontAwesomeIcon icon={["fas", "link"]} className="fa-lg" />
              </i>
              <a
                href={`${business.business ? business.business.web_link : "#"}`}
              >
                {business.business ? business.business.web_link : null}
              </a>
            </span>
          </div>
          <div className="d-flex flex-column ml-auto">
            {business.business && business.business.isApproved === true ? (
              <span className="ml-auto badge badge-success py-2 px-1">
                Approved
              </span>
            ) : (
              <span className="ml-auto badge badge-warning py-2 px-1">
                Pending
              </span>
            )}
          </div>
        </div>
      </div>
      <div className="border-bottom p-2 mb-2 d-flex align-items-center justify-content-center">
        <Link to="#" className="mr-5" disabled>
          <i>
            <FontAwesomeIcon icon={["fas", "check"]} />
          </i>
          Approve it
        </Link>
        <i style={{ cursor: "pointer" }} className="text-warning">
          <FontAwesomeIcon icon={["fas", "trash-alt"]} />
          Remove it
        </i>
      </div>
      <div className="d-flex flex-column justify-content-center align-items-center">
        {business.business ? (
          <img
            src={business.business.logo}
            width="100"
            height="100"
            alt="business-logo"
          />
        ) : null}
        <h3>{business.business ? business.business.name : null}</h3>
      </div>
      <div>
        <p>{business.business ? business.business.description : null}</p>
      </div>
    </div>
  );
};

export default ReadBusiness;
