import React from "react";
import MainLayout from "../../Layouts/MainLayout";
import TableInquiry from "../TableInquiry";

const HomeInquiryComponent = () => {
  return (
    <MainLayout>
      <div className="d-flex flex-column ml-4 mr-1">
        <h3>All Inquiries Request</h3>
        <TableInquiry />
      </div>
    </MainLayout>
  );
};

export default HomeInquiryComponent;
