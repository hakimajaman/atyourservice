import React, { useState } from "react";
import { useSelector } from "react-redux";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import ReactHtmlParser from "react-html-parser";
import { Button } from "react-bootstrap";

const FormArticle = () => {
  const article = useSelector((state) => state.ArticleReducer);
  const [value, setValue] = useState(article.article.data.body);
  const [subject, setSubject] = useState({
    title: article.article.data.title,
    category: article.article.data.category[0],
  });
  const handleChange = (e, editor) => {
    const data = editor.getData();
    setValue(data);
  };
  const handleChangeSubject = (e) => {
    setSubject({
      ...subject,
      [e.target.name]: e.target.value,
    });
  };
  return (
    <div className="d-flex flex-column flex-md-row">
      <div className="mb-4 mr-3">
        <div>
          <h4>Edit Your Article Here</h4>
        </div>
        <div className="d-flex mb-2">
          <input
            type="text"
            value={subject.title}
            name="title"
            onChange={handleChangeSubject}
            placeholder="Article Title"
          />
          <input
            type="text"
            name="category"
            value={subject.category}
            onChange={handleChangeSubject}
            placeholder="Article Categories"
          />
          <Button>Update Now</Button>
        </div>
        <CKEditor editor={ClassicEditor} onChange={handleChange} data={value} />
      </div>
      <div>
        <h4>Live Preview</h4>
        <div
          className="border border-dark bg-light"
          style={{ width: "20rem", height: "29rem", borderRadius: "10px" }}
        >
          <div
            className="bg-dark text-light text-center mb-3"
            style={{
              borderTopLeftRadius: "10px",
              borderTopRightRadius: "10px",
            }}
          >
            <span>My Phone</span>
          </div>
          <div style={{ overflow: "scroll", height: "90%", width: "100%" }}>
            <div className="text-center border-bottom">
              <h4>{ReactHtmlParser(subject.title)}</h4>
            </div>
            <div className="ml-2">{ReactHtmlParser(value)}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormArticle;
