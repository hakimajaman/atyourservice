import React, { useState, useEffect } from "react";
import "./assets/sass/LoginComponent.scss";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { LoginUser } from "../../store/actions/users";
import { Form, Button, Spinner } from "react-bootstrap";

const LoginComponent = () => {
  const pushTo = useHistory();
  const dispatch = useDispatch();
  const loggedStatus = useSelector((state) => state.loggedReducer);
  const [validated, setValidated] = useState(false);
  const [haveSigned, setHaveSigned] = useState("none");
  const [user, setUser] = useState({
    email: "",
    password: "",
  });

  const change = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    setValidated(true);
    if (form.checkValidity() === true) {
      event.preventDefault();
      dispatch(LoginUser(user.email, user.password));
    }
  };

  useEffect(() => {
    if (!loggedStatus && !localStorage.getItem("bearer")) {
      setHaveSigned("block");
    } else {
      setHaveSigned("none");
      pushTo.push("/admin/dashboard");
    }
  }, [loggedStatus, pushTo]);

  return (
    <div style={{ display: `${haveSigned}` }}>
      <div className="customLogin d-flex justify-content-center align-items-center">
        <div className="customLogin-form col-md-4 ml-3 mr-3 rounded pb-4 pt-3 shadow p-3 mb-5 bg-white">
          <div className="customSidebar-heading align-items-center justify-content-center d-flex flex-column">
            <img
              src={require("./assets/images/bukusaku-logo.png")}
              width="70px"
              height="70px"
              alt="logo"
            />
            <h5 className="mt-2 mb-4">Login to Dashboard</h5>
          </div>
          <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Email"
                name="email"
                onChange={change}
                value={user.email}
                required
              />
              <Form.Control.Feedback type="invalid">
                Email cannot be empty.
              </Form.Control.Feedback>
              {/*loginUserStatus === false
                ?
                <Form.Text style={{color: 'red'}}>
                  Invalid email or password.
              </Form.Text>
                :
                null
              */}
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                name="password"
                onChange={change}
                value={user.password}
                required
              />
              <Form.Control.Feedback type="invalid">
                Password cannot be empty.
              </Form.Control.Feedback>
              {/*loginUserStatus === false
                ?
                <Form.Text style={{color: 'red'}}>
                  Invalid email or password.
              </Form.Text>
                :
                null
              */}
            </Form.Group>
            <div className="d-flex justify-content-center align-items-center">
              <Button variant="danger" type="submit">
                Log in
              </Button>
              {loggedStatus ? (
                <Spinner animation="border" variant="danger" className="ml-3" />
              ) : null}
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default LoginComponent;
