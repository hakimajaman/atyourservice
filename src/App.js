import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import LoginComponent from "./components/DashboardPage/LoginComponent";

import MainLayout from "./components/DashboardPage/Layouts/MainLayout";
import DashboardComponent from "./components/DashboardPage/BodyComponents/Dashboard/DashboardComponent";
import HomeBusinessComponent from "./components/DashboardPage/BodyComponents/Business/HomeBusinessComponent";
import ViewBusinessComponent from "./components/DashboardPage/BodyComponents/Business/ViewBusinessComponent";
import MailBusinessComponent from "./components/DashboardPage/BodyComponents/Business/MailBusinessComponent";
import ReadBusiness from "./components/DashboardPage/BodyComponents/ReadBusiness";
import HomeArticleComponent from "./components/DashboardPage/BodyComponents/Article/HomeArticleComponent";
import EditArticleComponent from "./components/DashboardPage/BodyComponents/Article/EditArticleComponent";
import AddArticleComponent from "./components/DashboardPage/BodyComponents/Article/AddArticleComponent";
import LandingPage from "./Component/LandingPage";

library.add(fas, fab);

function App() {
  return (
    <BrowserRouter>
      <Route path="/admin">
        <MainLayout>
          <Route path="/admin/dashboard" exact>
            <DashboardComponent />
          </Route>
          <Route path="/admin/mail" exact>
            <MailBusinessComponent />
          </Route>
          <Route path="/admin/business" exact>
            <HomeBusinessComponent />
          </Route>
          <Route path="/admin/business/:id" exact>
            <ViewBusinessComponent />
          </Route>
          <Route path="/admin/read" exact>
            <ReadBusiness />
          </Route>
          <Route path="/admin/article" exact>
            <HomeArticleComponent />
          </Route>
          <Route path="/admin/edit/article/:id" exact>
            <EditArticleComponent />
          </Route>
          <Route path="/admin/add/article" exact>
            <AddArticleComponent />
          </Route>
        </MainLayout>
      </Route>
      <Route path="/login" exact>
        <LoginComponent />
      </Route>
      <Route path="/" exact>
        <LandingPage />
      </Route>
    </BrowserRouter>
  );
}

export default App;
