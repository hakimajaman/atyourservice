import {
  url,
  POST_LOGIN_USER,
  POST_LOGOUT_USER,
  GET_MY_PROFILE,
  GET_ALL_USERS,
  HIDE_LOADING_DASHBOARD
} from './types';

export const LoginUser = (email, password) => async (dispatch) => {
  try {
    const thisData = {
      email: email,
      password: password
    }
    const send = await fetch(`${url}/user/login`, {
      method: 'POST',
      headers: {
        'accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(thisData)
    })
    const res = await send.json()
    localStorage.setItem('bearer', res.data.token)
    dispatch({
      type: POST_LOGIN_USER
    })
  } catch (err) {
    dispatch({
      type: POST_LOGOUT_USER
    })
  }
}

export const GetMyProfile = () => async (dispatch) => {
  try {
    const get = await fetch(`${url}/user/userProfile`, {
      headers: {
        'accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('bearer')
      }
    })
    const res = await get.json()
    dispatch({
      type: GET_MY_PROFILE,
      payload: res.data
    })
  } catch (err) {
    console.log(err)
  }
}

export const GetAllUsers = () => async (dispatch) => {
  try {
    const get = await fetch(`${url}/user/getAndCount`, {
      headers: {
        'accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('bearer')
      }
    })
    const res = await get.json()
    dispatch({
      type: GET_ALL_USERS,
      payload: res
    })
    dispatch({
      type: HIDE_LOADING_DASHBOARD
    })
  } catch (err) {
    console.log(err)
  }
}
