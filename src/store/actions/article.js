import { url, GET_ALL_ARTICLES, GET_ARTICLE_BY_ID } from "./types";

export const GetAllArticles = () => async (dispatch) => {
  try {
    const get = await fetch(`${url}/article`, {
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("bearer"),
      },
    });
    const res = await get.json();
    dispatch({
      type: GET_ALL_ARTICLES,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const GetArticleById = (id) => async (dispatch) => {
  try {
    const get = await fetch(`${url}/article/details/${id.id}`, {});
    const res = await get.json();
    dispatch({
      type: GET_ARTICLE_BY_ID,
      payload: res,
    });
  } catch (err) {
    console.log(err);
  }
};

export const PostArticle = (title, body, category, image) => async (
  dispatch
) => {
  try {
    const formData = new FormData();
    formData.append("title", title);
    formData.append("body", body);
    formData.append("category", category);
    formData.append("image", image);
    console.log(image);
    console.log(formData.values());
    const send = await fetch(`${url}/article/addArticle`, {
      method: "POST",
      headers: {
        accept: "application/json",
        "Content-Type": "multipart/form-data",
        Authorization: localStorage.getItem("bearer"),
      },
      body: formData,
    });
    const res = await send.json();
    console.log(res);
  } catch (err) {
    console.log(err);
  }
};
