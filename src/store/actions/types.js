export const url = "https://buku-saku-glints.herokuapp.com/api/v1";

//Loading
export const SHOW_LOADING = "SHOW_LOADING";
export const SHOW_LOADING_DASHBOARD = "SHOW_LOADING_DASHBOARD";

export const HIDE_LOADING = "HIDE_LOADING";
export const HIDE_LOADING_DASHBOARD = "HIDE_LOADING_DASHBOARD";

//Logged
export const POST_LOGIN_USER = "POST_LOGIN_USER";
export const POST_LOGOUT_USER = "POST_LOGOUT_USER";

//Users
export const GET_MY_PROFILE = "GET_MY_PROFILE";
export const GET_ALL_USERS = "GET_ALL_USERS";

//Business
export const GET_ALL_BUSINESS = "GET_ALL_BUSINESS";
export const GET_BUSINESS_BY_ID = "GET_BUSINESS_BY_ID";
export const APPROVED = "APPROVED";
export const DELETE_BUSINESS = "DELETE_BUSINESS";

//Inquiry
export const GET_ALL_INQUIRIES = "GET_ALL_INQUIRIES";

//Report
export const GET_ALL_REPORTS = "GET_ALL_REPORTS";
export const GET_REPORT_BY_ID = "GET_REPORT_BY_ID";

//Article
export const GET_ALL_ARTICLES = "GET_ALL_ARTICLES";
export const GET_ARTICLE_BY_ID = "GET_ARTICLE_BY_ID";
