import { url, GET_ALL_REPORTS, GET_REPORT_BY_ID } from "./types";

export const GetAllReports = () => async (dispatch) => {
  try {
    const get = await fetch(`${url}/support`, {
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("bearer"),
      },
    });
    const res = await get.json();
    dispatch({
      type: GET_ALL_REPORTS,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const GetReportById = (id) => async (dispatch) => {
  try {
    const get = await fetch(`${url}/support?_id=${id}`, {
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("bearer"),
      },
    });
    const res = await get.json();
    console.log(res);
    dispatch({
      type: GET_REPORT_BY_ID,
      payload: res.data._id,
    });
  } catch (err) {
    console.log(err);
  }
};
