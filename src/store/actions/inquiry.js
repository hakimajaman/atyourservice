import {url, GET_ALL_INQUIRIES} from './types';

export const GetAllInquiries = () => async (dispatch) => {
  try {
    const get = await fetch(`${url}/inquiry`, {
      headers: {
        'accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('bearer')
      }
    });
    const res = await get.json();
    dispatch({
      type: GET_ALL_INQUIRIES,
      payload: res.data
    })
  } catch (err) {
    console.log(err)
  }
}

export const GetInquiryById = (id) => async (dispatch) => {
  try {
    const get = await fetch(`${url}/inquiry?_id=${id}`, {
      headers: {
        'accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('bearer')
      }
    });
    const res = await get.json();
    console.log(res);
  } catch (err) {
    console.log(err)
  }
}
