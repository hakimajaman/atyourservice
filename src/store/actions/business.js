import {
  url,
  GET_ALL_BUSINESS,
  GET_BUSINESS_BY_ID,
  APPROVED,
  DELETE_BUSINESS,
} from "./types";

export const GetAllBusiness = () => async (dispatch) => {
  try {
    const get = await fetch(`${url}/business`, {
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("bearer"),
      },
    });
    const res = await get.json();
    dispatch({
      type: GET_ALL_BUSINESS,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const GetBusinessById = (id) => async (dispatch) => {
  try {
    const get = await fetch(`${url}/business?_id=${id.id}`, {
      headers: {
        accept: "application/json",
        Authorization: localStorage.getItem("bearer"),
      },
    });
    const res = await get.json();
    dispatch({
      type: GET_BUSINESS_BY_ID,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
    dispatch({
      type: GET_BUSINESS_BY_ID,
      payload: err,
    });
  }
};

export const ApproveBusiness = (id) => async (dispatch) => {
  try {
    const get = await fetch(`${url}/business/${id}`, {
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("bearer"),
      },
    });
    const res = await get.json();
    console.log(res);
    dispatch({
      type: APPROVED,
      payload: res.data._id,
    });
  } catch (err) {
    console.log(err);
  }
};

export const DeleteBusiness = (id) => async (dispatch) => {
  try {
    const deleteIt = await fetch(`${url}/business/${id}`, {
      method: "DELETE",
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("bearer"),
      },
    });
    const res = await deleteIt.json();
    dispatch({
      type: DELETE_BUSINESS,
      payload: res.data._id,
    });
  } catch (err) {
    console.log(err);
  }
};
