import {
  GET_ALL_BUSINESS,
  GET_BUSINESS_BY_ID,
  APPROVED,
  DELETE_BUSINESS,
} from "../actions/types";

const businessReducer = (state = {}, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_ALL_BUSINESS:
      return { ...state, allBusiness: payload };
    case GET_BUSINESS_BY_ID:
      return { ...state, business: payload };
    case APPROVED:
      return {
        ...state,
        allBusiness: state.allBusiness.map((item) =>
          item._id === payload ? (item = { ...item, isApproved: true }) : item
        ),
      };
    case DELETE_BUSINESS:
      return {
        ...state,
        allBusiness: state.allBusiness.filter((item) => item._id !== payload),
      };
    default:
      return {
        ...state,
      };
  }
};

export default businessReducer;
