import {GET_MY_PROFILE, GET_ALL_USERS} from '../actions/types';

const UserReducer = (state = [], action) => {
  const {type, payload} = action
  switch (type) {
    case GET_MY_PROFILE:
      return {...state, profile: payload}
    case GET_ALL_USERS:
      return {...state, allUsers: payload}
    default:
      return state
  }
}

export default UserReducer;
