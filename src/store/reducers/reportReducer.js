import { GET_ALL_REPORTS, GET_REPORT_BY_ID } from "../actions/types";

const reportReducer = (state = {}, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_ALL_REPORTS:
      return { ...state, allReports: payload };
    case GET_REPORT_BY_ID:
      return {
        ...state,
        allReports: state.allReports.map((item) =>
          item._id === payload ? (item = { ...item, isRead: true }) : item
        ),
      };
    default:
      return {
        ...state,
      };
  }
};

export default reportReducer;
