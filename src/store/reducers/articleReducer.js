import { GET_ALL_ARTICLES, GET_ARTICLE_BY_ID } from "../actions/types";

const articleReducer = (state = {}, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_ALL_ARTICLES:
      return { ...state, allArticles: payload };
    case GET_ARTICLE_BY_ID:
      return { ...state, article: payload };
    default:
      return { ...state };
  }
};

export default articleReducer;
