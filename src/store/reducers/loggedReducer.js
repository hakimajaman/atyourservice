import {POST_LOGIN_USER, POST_LOGOUT_USER} from '../actions/types';

const LoggedReducer = (state = false, action) => {
  const {type} = action
  switch (type) {
    case POST_LOGIN_USER:
      return state = true
    case POST_LOGOUT_USER:
      return state = false
    default:
      return state
  }
}

export default LoggedReducer;
