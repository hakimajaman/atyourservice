import {GET_ALL_INQUIRIES} from '../actions/types';

const InquiryReducer = (state = [], action) => {
  const {type, payload} = action
  switch (type) {
    case GET_ALL_INQUIRIES:
      return {...state, allInquiries: payload}
    default:
      return state
  }
}

export default InquiryReducer;
