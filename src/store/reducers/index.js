import { combineReducers } from "redux";
import loggedReducer from "./loggedReducer";
import UserReducer from "./userReducer";
import BusinessReducer from "./businessReducer";
import InquiryReducer from "./inquiryReducer";
import ReportReducer from "./reportReducer";
import ArticleReducer from "./articleReducer";
import {
  SHOW_LOADING_DASHBOARD,
  HIDE_LOADING_DASHBOARD,
} from "../actions/types";

const LoadingReducer = (state = true, action) => {
  const { type } = action;
  switch (type) {
    case SHOW_LOADING_DASHBOARD:
      return !state;
    case HIDE_LOADING_DASHBOARD:
      return state;
    default:
      return state;
  }
};

export default combineReducers({
  LoadingReducer,
  loggedReducer,
  UserReducer,
  BusinessReducer,
  InquiryReducer,
  ReportReducer,
  ArticleReducer,
});
