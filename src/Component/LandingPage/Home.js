import React from "react";

import MockupHome from "../../Assets/mockup-home.png";
const Home = props => {
  return (
    <div id="home">
      <div className="container-fluid">
        <div className=" d-flex flex-wrap flex-sm-nowrap justify-content-space-around align-items-center">
          <div className="row justify-content-center">
            <div className="col-md-7 col-sm-7 text-center ">
              <h1>
                Bring your business, lifestyle, and knowledge to the next level
              </h1>
            </div>
          </div>
          <div className="row col-md-5 col-sm-5 justify-content-center ">
            <img
              src={MockupHome}
              alt="bukusaku"
              className="gambar"
              width="300"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
