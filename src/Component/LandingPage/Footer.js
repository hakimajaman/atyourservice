import React, { Component } from "react";
export class Footer extends Component {
  render() {
    return (
      <div>
        <div className="footer">
          <div className="container-fluid">
            <div className="row">
              <div class="col-md-4 col-sm-6 col-xs-6">
                <i class="fas fa-map-marker"></i>
                <p>
                  Graha sakura 1st Floor, Jalan kebenaran Telp. 123 - 456
                  <br />
                  Yogyakarta, Jawa Tengah 57615, Indonesia{" "}
                </p>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-6">
                <p>
                  <i class="fas fa-envelope"></i>
                  hello@bukusaku.com
                </p>
                <p>
                  <i class="fas fa-phone"></i>
                  +62 1234567890
                </p>
              </div>
              <div class="col-md-4 col-sm-12 col-xs-12 text-right">
                <div className="socmed">
                  <a href>
                    <i class="fa fab fa-instagram"></i>
                  </a>
                  <a href>
                    <i class="fa fab fa-facebook"></i>
                  </a>
                  <a href>
                    <i class="fa fab fa-twitter"></i>
                  </a>
                </div>
              </div>
            </div>
            <div className="row justify-content-end">
              <div className="col-md-5 text-right">
                <p>© Bukusaku. All Right Reserved</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
