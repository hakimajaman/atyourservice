import React, { Component } from "react";
import "../../Styles/LandingPage.css";
import image1 from "../../Assets/logo.png";
import image2 from "../../Assets/binar-logo.png";
import image3 from "../../Assets/adidas-logo.png";

export class AboutUs extends Component {
  render() {
    return (
      <div>
        <div className="about">
          <div className="container-fluid">
            <div className="row">
              <div class="col-md-6 text-center about-us">
                <h5>About Us</h5>
                <img src={image1} alt="" />
                <p>
                  We here with one purpose, to leverage people’s standard of
                  living. We started it with a small step, to provide a better
                  life and economy for people who life in rural.
                </p>
              </div>
              <div class="col-md-6 text-center partner">
                <h5>Partnership</h5>
                <img src={image2} alt="" />
                <img src={image3} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AboutUs;
