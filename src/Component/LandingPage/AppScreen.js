import React, { Component } from "react";
import image1 from "../../Assets/mockup-appscreen-1.png";
import image2 from "../../Assets/mockup-appscreen-2.png";
import image3 from "../../Assets/mockup-appscreen-3.png";
import image4 from "../../Assets/mockup-appscreen-4.png";
import image5 from "../../Assets/mockup-appscreen-5.png";

export class AppScreen extends Component {
  render() {
    return (
      <div>
        <div className="appscreen ">
          <div className="container-fluid">
            <div className="row">
              <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h3>AppScreen</h3>
                <div>
                  <img src={image1} alt="" />
                  <img src={image2} alt="" />
                  <img src={image3} alt="" />
                  <img src={image4} alt="" />
                  <img src={image5} alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AppScreen;
