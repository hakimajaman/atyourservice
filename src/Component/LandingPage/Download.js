import React, { Component } from "react";
import "../../Styles/LandingPage.css";
import image1 from "../../Assets/mockup-large.png";
// import image2 from "../../Assets/mockup-small.png";
import image3 from "../../Assets/gplay.png";

export class Download extends Component {
  render() {
    return (
      <div>
        <div className="download">
          <div className="container-fluid">
            <div className="text-center">
              <h1>You can find us on</h1>
            </div>
            <div className="row">
              <div
                id="left-download"
                className="col-md-6 col-sm-4 col-xs-12 ml-md-5 mx-auto text-center "
              >
                <img src={image1} alt="" />
                {/* <img src={image2} alt="" className="d-none d-lg-block" /> */}
              </div>
              <div id="right-download" className="col-md-5 col-sm-8 col-xs-12 ">
                <div className="text-download mt-4">
                  <h5>Lorem ipsum dolor sit amet</h5>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Mauris dignissim consequat lectus, eu ultrices nisi porta
                    nec. Quisque eu euismod libero
                  </p>
                </div>
                <div className="google-button mx-auto text-center">
                  <img src={image3} alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Download;
