import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";
import "../../Styles/LandingPage.css";
import Logo from "../../Assets/AYS.png";

const Header = props => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className="pb-5">
      <div className="pb-5 ">
        <Navbar
          light
          className="fixed-top  mr-xl-5 ml-xl-5  header"
          expand="md"
        >
          <NavbarBrand href="/">
            <img src={Logo} alt="bukusaku" height="30" />
          </NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="navbar-nav ml-auto " navbar>
              <NavItem className="nav-item ">
                <NavLink href="home">Home</NavLink>
              </NavItem>
              <NavItem className="nav-item">
                <NavLink href="">Features</NavLink>
              </NavItem>
              <NavItem className="nav-item">
                <NavLink href="">Download</NavLink>
              </NavItem>
              <NavItem className="nav-item">
                <NavLink href="">About Us</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    </div>
  );
};

export default Header;
