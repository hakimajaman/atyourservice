import React, { Component } from "react";
import "../../Styles/LandingPage.css";

export class ContactSupport extends Component {
  render() {
    return (
      <div>
        <div className="contact">
          <div className="container-fluid">
            <div className="row justify-content-center">
              <div className="col-md-6 col-sm-8 col-xs-8 text-center pt-4 pb-4">
                <h3> Contact & Support</h3>
                <h5>
                  If you have any question, contact us for more information
                </h5>
                <form>
                  <input
                    type="text"
                    placeholder="Your Name"
                    className="form-control"
                  />
                  <input
                    type="text"
                    placeholder="Your Email"
                    className="form-control"
                  />
                  <textarea
                    cols="30"
                    rows="10"
                    placeholder="Your Feedback"
                    className="form-control"
                  />
                  <button className="button secondary-color">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ContactSupport;
