import React, { Component } from "react";
import "../../Styles/LandingPage.css";
// import image1 from "../../Assets/mockup-feature-1.png";
// import image2 from "../../Assets/mockup-feature-2.png";
import image3 from "../../Assets/findyour.png";
import image4 from "../../Assets/keepand.png";
import image5 from "../../Assets/latest.png";
export class Feature extends Component {
  render() {
    return (
      <div className="pt-xl-4 pb-xl-4 pt-md-3 pb-md-3">
        <div className="pt-xl-4 pb-xl-4 pt-md-3 pb-md-3">
          <div className="feature">
            <div style={{ paddingTop: "0px" }} className="container-fluid">
              <div className="text-center">
                <h1>Features</h1>
                {/* <h5></h5> */}
              </div>
              <div className="row row-cols-1 row-cols-sm-2 row-cols-md-4 text-center justify-content-md-center">
                <div className="col">
                  <img src={image3} alt="" />
                  <h5>Find Your Business Partner</h5>
                  <p>
                    We bring oportunity for you who want to develop their
                    business. One of our services is to help you find business
                    partner
                  </p>
                </div>
                <div className="col">
                  <img src={image4} alt="" />
                  <h5>Keep and Share Anywhere, Anytime</h5>
                  <p>You can save and share your items for everyone</p>
                </div>
                <div className="col">
                  <img src={image5} alt="" />
                  <h5>
                    Latest information about busineses, lifestyle, and
                    technology
                  </h5>
                  <p>
                    Find many information and references that fit your need in
                    one hand
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Feature;
