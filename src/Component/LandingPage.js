import React from "react";
import Header from "./LandingPage/Headers";
import Home from "./LandingPage/Home";
import Feature from "./LandingPage/Feature";
import Download from "./LandingPage/Download";
import AboutUs from "./LandingPage/AboutUs";
import AppScreen from "./LandingPage/AppScreen";
import ContactSupport from "./LandingPage/ContactSupport";
import Footer from "./LandingPage/Footer";
import "../Styles/LandingPage.css";

const LandingPage = props => {
  return (
    <div>
      <Header />
      <Home />
      <Feature />
      <Download />
      <AboutUs />
      <AppScreen />
      <ContactSupport />
      <Footer />
    </div>
  );
};

export default LandingPage;
